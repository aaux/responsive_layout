 /* ===================================================
  * main.js - Main Behaviors
  * =================================================== */

 // GLOBAL VARIABLES
 var global = {};

 global = {
      init: function () {
           global.resizeShellBehaviors();
           global.toggleDetailsPanel();
      },
      // RESIZE
      ///////////////
      resizeShellBehaviors: function () {

          if ( $(window).width() < 1280 ) {
               // $(".panel__details").toggleClass("open");
          }

          $(window).resize(function() {
               if ( $(window).width() > 1280 ) {
                    $('.panel__details').removeClass('open');
               }
          });
      },
      toggleDetailsPanel: function () {
          var detailsPanel = $('.panel__details');
          $('.toggle-panel').on('click', function() {
               if( $(window).width() > 1280 ) {
                    detailsPanel.toggleClass('closed');
               } else {
                    detailsPanel.toggleClass('open');
               }
          });
      }
 };

 $(global.init);